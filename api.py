from flask import Flask, jsonify, request, g
import sqlite3
import threading
import boto3
import json
import ssl
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient

app = Flask(__name__)

# Configuración de la base de datos SQLite
DATABASE = 'sensor_database.db'

session = boto3.Session(profile_name='default')
credentials = session.get_credentials()
access_key = credentials.access_key
secret_key = credentials.secret_key
region = session.region_name

# Configura la conexión MQTT
client_id = "mi-cliente-python"
iot_endpoint = "arn:aws:iot:us-east-2:825712769836:thing/sensor"

client = AWSIoTMQTTClient(client_id)
client.configureEndpoint(iot_endpoint, 8883)
client.configureCredentials(
    CAFilePath="root-CA.crt",
    KeyPath="private.pem.key",
    CertificatePath="cert.pem.crt"
)


def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(DATABASE)
    return g.db


@app.route('/api/sensor', methods=['GET'])
def get_sensor_data():
    conn = sqlite3.connect('sensor_database.db')
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM sensor_data')
    rows = cursor.fetchall()

    sensor_data = []
    for row in rows:
        data = {
            'id': row[0],
            'humidity': row[1],
            'temperature': row[2],
            'timestamp': row[3]
        }
        sensor_data.append(data)

    conn.close()

    return jsonify(sensor_data)


@app.route('/api/sensor', methods=['POST'])
def receive_sensor_data():
    data = request.get_json()
    humidity = data.get('humidity')
    temperature = data.get('temperature')

    with app.app_context():
        db = get_db()
        cursor = db.cursor()
        cursor.execute('INSERT INTO sensor_data (humidity, temperature) VALUES (?, ?)',
                       (humidity, temperature))
        db.commit()

    sensor = {
        "humidity": humidity,
        "temperature": temperature
    }

    return jsonify(sensor)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
